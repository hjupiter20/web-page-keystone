var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var ProductSubSubCategory = new keystone.List('ProductSubSubCategory', {
	autokey: { from: 'name', path: 'slug', unique: true },
});

ProductSubSubCategory.add({
	name: { type: String, required: true },
  product: { type: Types.Relationship, ref: 'Product', many: true },
});

//ProductSubSubCategory.relationship({ ref: 'Product', path: 'products', refPath: 'productSubSubCategory' });
ProductSubSubCategory.defaultColumns = 'name, productSubCategory';
ProductSubSubCategory.register();
