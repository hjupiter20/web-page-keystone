var keystone = require('keystone');
var Types = keystone.Field.Types;

var Product = new keystone.List('Product',{
  map: {name: 'title'},
  singular: 'Product',
  plural: 'Products',
  autokey: {path:'slug', from:'title', unique:true}
});

var myStorage = new keystone.Storage({
  adapter: keystone.Storage.Adapters.FS,
  fs: {
    path: 'data/productos',
    publicPath: '/productos',
  },
});

Product.add({
  title: {type: String, required: true},
  //price: {type: Number},
  //qty: {type: Number},
  description: {type: Types.Html, wysiwyg: true, height: 300},
  image: {type: Types.CloudinaryImage},
  publishedDate: {type: Date, default: Date.now},
  //productSubSubCategory: {type: Types.Relationship, ref: 'ProductSubSubCategory', many: true },
  someFile: { type: Types.File, storage: myStorage },
});

Product.defaultColumns = 'title, productSubSubCategory';
Product.register();
