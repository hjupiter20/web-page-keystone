var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var ProductCategory = new keystone.List('ProductCategory', {
	autokey: { from: 'name', path: 'slug', unique: true },
});

ProductCategory.add({
	name: { type: String, required: true },
	image:{ type: Types.CloudinaryImage},
	content: {
		description: { type: Types.Html, wysiwyg: true, height: 400 },
	},
	productGroup: { type: Types.Relationship, ref: 'ProductGroup', many: true}
});

//ProductCategory.relationship({ ref: 'ProductGroup', path: 'ProductGroup', refPath: 'productCategories' });
ProductCategory.defaultColumns = 'name';
ProductCategory.register();
