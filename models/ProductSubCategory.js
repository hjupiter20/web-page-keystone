var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var ProductSubCategory = new keystone.List('ProductSubCategory', {
	autokey: { from: 'name', path: 'slug', unique: true },
});

ProductSubCategory.add({
	name: { type: String, required: true },
  productSubSubCategory: { type: Types.Relationship, ref: 'ProductSubSubCategory', many: true },
	product: { type: Types.Relationship, ref: 'Product', many: true },
});

//ProductSubCategory.relationship({ ref: 'ProductSubSubCategory', path: 'productSubSubCategory', refPath: 'productSubCategory' });
ProductSubCategory.defaultColumns = 'name, productGroup';
ProductSubCategory.register();
