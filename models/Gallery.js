var keystone = require('keystone');
var Types = keystone.Field.Types;

var Gallery = new keystone.List('Gallery',{
  autokey: {from:'title', path:'slug', unique:'true'},
  map: { name: 'title' },
  defaultSort: '-createdAt'
});

Gallery.add({
  title:{type: String, required: true},
  image:{type: Types.CloudinaryImage},
  content: {
      brief: { type: Types.Html, wysiwyg: true, height: 150 }
  },
  createdAt:{type: Date, default: Date.now},
  updateAt:{type: Date, default: Date.now},
  state: { type: Types.Boolean },
});


/**
 * Registration
 */
Gallery.defaultColumns = 'title, ,state ,createdAt, updateAt';
Gallery.register();
