var keystone = require('keystone');
var Types = keystone.Field.Types;

/**
 * PostCategory Model
 * ==================
 */

var ProductGroup = new keystone.List('ProductGroup', {
	autokey: { from: 'name', path: 'slug', unique: true },
});

ProductGroup.add({
	name: { type: String, required: true },
	image:{ type: Types.CloudinaryImages},
  productSubCategories: { type: Types.Relationship, ref: 'ProductSubCategory', many: true },
	product: { type: Types.Relationship, ref: 'Product', many: true },
});

//ProductGroup.relationship({ ref: 'ProductSubCategory', path: 'productSubCategory', refPath: 'productGroup' });
ProductGroup.defaultColumns = 'name, productCategories';
ProductGroup.register();
