var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'plastico';
	locals.filters = {
		category: req.params.category,
	};
	locals.data = {
		group:[],
		categories: [],
		product: [],
		mostrar: [],
		mostrarProducto : [],
	};

	view.on('init', function(next){
		var q = keystone.list('ProductCategory').model.find({slug:'plasticos'});
		q.exec(function(err,results){
			locals.data.group = results;
			//console.log("7777777777777777777777777777777777777777777777");
			//console.log(results);
			//console.log(results[0]["id"]);
			//console.log("7777777777777777777777777777777777777777777777");
			next();
		});
	});

	// Load all categories
	view.on('init', function (next) {
		//console.log("444444444444444444444444444444444444444444");
		//console.log(locals.data.group.id);
		//console.log("444444444444444444444444444444444444444444");
		keystone.list('ProductCategory').model.find({slug:'plasticos'}).populate({ path: 'productGroup'}).exec(function (err, results) {
			if (err || !results.length) {
				return next(err);
			}
			//locals.data.categories = results;
			//console.log("*****************************************");
			//console.log(results[0]["productGroup"]);
			locals.data.categories = results[0]["productGroup"];
			//console.log("*****************************************");
			// Load the counts for each category
			next();
		});
	});

	// Load the current category filter
	view.on('init', function (next) {
		if (req.params.category) {
			keystone.list('ProductGroup').model.findOne({ slug: locals.filters.category }).exec(function (err, result) {
				locals.data.category = result;
				//console.log(result.image);
				//locals.data.name = "Noticia - "+capitalizeFirstLetter(req.params.category);
				//console.log("66666666666666666666666666666");
				//console.log(result);
				//console.log(locals.data.category);
				//console.log("66666666666666666666666666666");
				next(err);
			});
		} else {
			next();
		}
	});


	view.on('init', function(next){
		//console.log("9999999999999999999999999999999999999");
		//console.log(locals.filters.category);
		keystone.list('ProductGroup').model.find({slug:locals.filters.category})
						.populate({ path: 'productSubCategories' ,populate: [{path: 'productSubSubCategory', populate: {path : 'product'}},{path: 'product'}]})
						.populate({ path: 'product' })
						.exec(function(err, results){
							if(err){
								throw err;
							}
							//console.log((results));
							//locals.data.mostrar = results[0]["productGroup"];
							//console.log((results[0]["productSubCategories"])[0]["productSubSubCategory"]);
							/*if (err || !(results[0]["productSubCategories"]).length) {
								return next(err);
							}*/
							//console.log(locals.data.category);
							if ((results[0]["product"]).length>0) {
								locals.data.mostrarProducto = results[0]["product"];
							}
							else{
								locals.data.mostrar = results[0]["productSubCategories"];
							}
							next();
						});
	});

	function test(a){
		//console.log(a);
	}

	//http://mongoosejs.com/docs/populate.html
	//https://stackoverflow.com/questions/32174803/mongoose-two-level-population-using-keystonejs


	// Render the view
	view.render('categoryPlastic');
};
