var keystone = require('keystone');

exports = module.exports = function (req, res) {

	var view = new keystone.View(req, res);
	var locals = res.locals;

	// locals.section is used to set the currently selected
	// item in the header navigation.
	locals.section = 'home';

	locals.data ={
		posts : [],
		categoria: String,
  }

	view.on('init', function(next){
		var q = keystone.list('Post').model.find().where('state', 'published').sort('-publishedDate');
		q.exec(function (err, results) {
			locals.data.posts = results;
			next(err);
			//console.log(results[0]['categories']);
			var t = results[0]['categories'];
			//console.log(t);
			var c = keystone.list('PostCategory').model.find().where('_id',t);
			c.exec(function (err, results){
				locals.data.categoria = results[0]['name'];
				//console.log(locals.data.categoria);
			});
		});


	});

	view.query('galleries', keystone.list('Gallery').model.find({ state: true }).sort('sortOrder'));

	// Render the view
	view.render('index');
};
