var keystone = require('keystone');
var async = require('async');

exports = module.exports = function (req, res) {
	var view = new keystone.View(req, res);
	var locals = res.locals;

	// Init locals
	locals.section = 'store';
	locals.filters = {
		category: req.params.category,
    groupParam: req.params.group
	};
	locals.data = {
		categoryData: [],
    groups: [],
    groupId: [],
    products: [],
    imagenesGrupos: []
	};

	// Load current category
	view.on('init', function (next) {
    var q = keystone.list('ProductCategory').model.findOne({
      slug: locals.filters.category
    });
    q.exec(function(err,results){
      locals.data.categoryData = results;
			//console.log("******************************************************");
			//console.log(results);
			//console.log("******************************************************");
      next();
    });
	});

  //load all groups in category
  view.on('init', function(next){
    var q = keystone.list('ProductGroup').model.find({
      productCategories: locals.data.categoryData.id
    });
    q.exec(function(err,results){
      locals.data.groups = results;
      for(var i = 0; i<results.length;i++){
				locals.data.imagenesGrupos.push(results[i]);
      }
      //console.log("______________________________________________________--");
      //console.log(locals.data.imagenesGrupos);
			//console.log(locals.data.imagenesGrupos[0].image);
      //console.log("______________________________________________________--");
      next();
    });
  });

  //load products in groups
  view.on('init', function(next){
    for(i=0;i<locals.data.groups.length;i++){
      locals.data.groupId[i] = locals.data.groups[i]["id"];
    }
    var q = keystone.list('Product').model.find({
      productGroup: { $in: locals.data.groupId }
    });
    q.exec(function(err,results){
      locals.data.products = results;
      //console.log(results);
      next();
    });
  });

  // Load the current group filter
	view.on('init', function (next) {
		if (req.params.group) {
      //console.log(locals.filter.groupParam);
      next();
		} else {
			next();
		}
	});

	// Render the view
	view.render('groupProducts');
}
