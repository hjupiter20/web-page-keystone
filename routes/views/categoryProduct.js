var keystone = require('keystone');

exports = module.exports = function(req, res){
  var view =  new keystone.View(req,res);
  var locals = res.locals;

  //Set locals
  locals.section = 'store';
  locals.data = {
		category:[],
	};

  //Load Products
  //view.query('products', keystone.list('Product').model.find());
  //view.query('productCategory', keystone.list('ProductCategory').model.find());

  view.on('init', function (next) {
    var q = keystone.list('ProductCategory').model.find().populate({path: 'productGroup'});

    q.exec(function(err,results){
      locals.data.category = results;
      //console.log(results);
      next();
    });
  });

  //Render view
  view.render('categoryProduct');
}
